const mongoose = require('mongoose');
const isEmail = require('validator').isEmail;


const InquirySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  ddd: {
    type: Number,
    required: true,
  },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    required: true,
    validate: [isEmail, 'invalid email'],
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true,
  },
});

mongoose.model('Inquiry', InquirySchema);
