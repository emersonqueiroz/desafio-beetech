const Plan = require('./Plan');
const Fee = require('./Fee');
const Inquiry = require('./Inquiry');

module.exports = {
  Plan,
  Fee,
  Inquiry,
};
