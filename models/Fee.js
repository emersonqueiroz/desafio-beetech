const mongoose = require('mongoose');

const FeeSchema = new mongoose.Schema({
  from: { type: Number },
  to: { type: Number },
  value: { type: Number },
});

mongoose.model('Fee', FeeSchema);
