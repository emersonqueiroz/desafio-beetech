const mongoose = require('mongoose');

const PlanSchema = new mongoose.Schema({
  name: { type: String, default: '' },
  minutes: { type: Number, default: 0 },
});
mongoose.model('Plan', PlanSchema);
