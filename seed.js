const path = require('path');
const seeder = require('mongoose-seed');
const data = require('./seeds');

var configFile = process.env.NODE_ENV === 'test' ? 'test.env' : 'dev.env';
var config = require(`./config/${configFile}`);

seeder.connect(config.DB, () => {
  seeder.loadModels([
    path.join(__dirname, 'models', 'Fee.js'),
    path.join(__dirname, 'models', 'Plan.js'),
  ]);

  seeder.clearModels(['Fee', 'Plan'], () => {
    seeder.populateModels(data, () => {
      seeder.disconnect();
    });
  });
});
