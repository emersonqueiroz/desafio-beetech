const Fee = require('mongoose').model('Fee');

module.exports = {
  get(req, res) {
    Fee.find({})
      .then(list => res.json(list))
      .catch(() => res.status(500).send({ error: 'Something went wrong' }));
  },
};

