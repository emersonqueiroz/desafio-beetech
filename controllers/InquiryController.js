const Inquiry = require('mongoose').model('Inquiry');

module.exports = {
  create(req, res) {
    const { name, email, ddd } = req.body;

    const inq = new Inquiry({ name, email, ddd });
    inq.save()
      .then(() => res.json({ success: true }))
      .catch(err => res.status(500).send({ error: err.message }));
  },
};

