const Plan = require('mongoose').model('Plan');

module.exports = {
  get(req, res) {
    Plan.find({})
      .then(list => res.json(list))
      .catch(() => res.status(500).send({ error: 'Something went wrong' }));
  },
};

