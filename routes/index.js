const FeeController = require('../controllers/FeeController');
const PlanController = require('../controllers/PlanController');
const InquiryController = require('../controllers/InquiryController');

module.exports = (app) => {
  app.get('/plans', PlanController.get);
  app.get('/fees', FeeController.get);
  app.post('/inquiry', InquiryController.create);
};
