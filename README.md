# desafio_beeteh

> Teste da BeeTech utilizando express.js e vue.js

## Dependencies
```
mongodb
node v8.7.0+
```

## Build Setup

``` bash
# install dependencies
npm install
or
yarn

# seed the database
npm run seed

# serve start at localhost:8080
npm start

# run all tests
npm test
```