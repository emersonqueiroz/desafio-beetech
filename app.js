var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var configFile = process.env.NODE_ENV === 'test' ? 'test.env' : 'dev.env';
var config = require(`./config/${configFile}`);

mongoose.connect(config.DB);
mongoose.Promise = Promise;

require('./models');
var routes = require('./routes');
var app = express();

app.use(logger('dev'));
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended':'false'}));
app.use(express.static(path.join(__dirname, 'dist')));
// app.use('/books', express.static(path.join(__dirname, 'dist')));
routes(app);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

module.exports = app;