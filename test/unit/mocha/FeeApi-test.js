/* eslint-env mocha */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-unused-expressions */
const chai = require('chai');

const expect = chai.expect;
const chaiHttp = require('chai-http');
const app = require('../../../app');

chai.use(chaiHttp);

describe('/GET fees', () => {
  it('it should return all the fees', (done) => {
    chai.request(app)
      .get('/fees')
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.a('array');
        expect(res.body[0]).to.have.property('from');
        expect(res.body[0]).to.have.property('to');
        expect(res.body[0]).to.have.property('value');
        done();
      });
  });
});
