/* eslint-env mocha */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-unused-expressions */
const chai = require('chai');

const expect = chai.expect;
const chaiHttp = require('chai-http');
const app = require('../../../app');

chai.use(chaiHttp);

describe('/POST inquiry', () => {
  it('it should save inquiry', (done) => {
    chai.request(app)
      .post('/inquiry')
      .send({
        name: 'Emerson',
        ddd: '11',
        email: 'equeiroz.sp@gmail.com',
      })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.have.property('success');
        done();
      });
  });

  it('it should return validation error (invalid DDD test)', (done) => {
    chai.request(app)
      .post('/inquiry')
      .send({
        name: 'Emerson',
        ddd: 'AA',
        email: 'equeiroz.sp@gmail.com',
      })
      .end((err, res) => {
        expect(res).to.have.status(500);
        expect(res).to.be.json;
        expect(res.body).to.have.property('error');
        done();
      });
  });

  it('it should return validation error (invalid e-mail test)', (done) => {
    chai.request(app)
      .post('/inquiry')
      .send({
        name: 'Emerson',
        ddd: '11',
        email: 'lorem',
      })
      .end((err, res) => {
        expect(res).to.have.status(500);
        expect(res).to.be.json;
        expect(res.body).to.have.property('error');
        done();
      });
  });
});
