import Api from '@/services/Api';

export default {
  fetchFees() {
    return Api().get('fees');
  },
};
