import Api from '@/services/Api';

export default {
  fetchPlans() {
    return Api().get('plans');
  },
};
