export default {
  leftPad(string, lenth, char) {
    let str = String(string);
    let i = 0;
    let ch = char;

    if (!char && char !== 0) ch = ' ';
    const len = lenth - str.length;
    while (i < len) {
      str = ch + str;
      i += 1;
    }
    return str;
  },
  formatMoney(n) {
    return `R$ ${n.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+)/g, '$1.')}`;
  },
};
