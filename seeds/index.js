const feesData = require('./fees');
const plansData = require('./plans');

module.exports = [
  {
    model: 'Fee',
    documents: feesData,
  },
  {
    model: 'Plan',
    documents: plansData,
  },
];
