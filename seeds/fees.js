module.exports = [
  {
    from: 11,
    to: 16,
    value: 1.9,
  },
  {
    from: 16,
    to: 11,
    value: 2.9,
  },
  {
    from: 11,
    to: 17,
    value: 1.7,
  },
  {
    from: 17,
    to: 11,
    value: 2.7,
  },
  {
    from: 11,
    to: 18,
    value: 0.9,
  },
  {
    from: 18,
    to: 11,
    value: 1.9,
  },
];
